#!/usr/bin/env python3

import argparse
import dokuwiki
import os
import sys


class FileUpload:
    file: str
    upload_path: str

    def __init__(self, path: str) -> None:
        split: list[str] = path.split("=")
        if not len(split) == 2:
            raise TypeError("Invalid file upload pair, must be file=upload-path")

        self.file = split[0]
        self.upload_path = split[1]


def main() -> int:
    args = parse_args()

    try:
        wiki = dokuwiki.DokuWiki(args.url, args.username, args.password)

        file_upload: FileUpload
        for file_upload in args.files:
            print(f"Uploading {file_upload.file} -> {file_upload.upload_path}")
            wiki.medias.add(file_upload.upload_path, file_upload.file, overwrite=True)

    except dokuwiki.DokuWikiError as dwe:
        print(f"Unable to connect: {dwe}")
    return 0


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Utility for uploading files to DokuWiki"
    )
    parser.add_argument(
        "--url", "-u", required=True, type=str, help="URL to the DokuWiki instance"
    )
    parser.add_argument(
        "--username", "-U", required=True, type=str, help="Username to connect as"
    )
    parser.add_argument(
        "--password",
        "-p",
        default=os.environ.get("DW_PASSWORD"),
        type=str,
        help="Password to connect with (can be set as environment variable DW_PASSWORD",
    )
    parser.add_argument(
        "--files",
        nargs="+",
        metavar="filename=upload-path",
        required=True,
        type=FileUpload,
        help="Files to upload",
    )

    return parser.parse_args()


if __name__ == "__main__":
    sys.exit(main())
